class Transaction {
  final String id;
  final String title;
  final double amount;
  final DateTime date;

  static int currentID = 0;

  Transaction({required this.title, required this.amount, DateTime? date})
      : date = date ?? DateTime.now(),
        id = "000${++currentID}";
}
