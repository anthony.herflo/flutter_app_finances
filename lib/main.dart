// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, avoid_print

import 'package:app_finanza_personal/classes/transaction.dart';
import 'package:app_finanza_personal/components/chart.dart';
import 'package:app_finanza_personal/components/transaction_form.dart';
import 'package:app_finanza_personal/components/transactions.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
        fontFamily: 'AlbertSans',
        textTheme: ThemeData.light().textTheme.copyWith(
              titleSmall: TextStyle(fontFamily: 'AlbertSans', fontSize: 13),
              titleMedium: TextStyle(fontFamily: 'AlbertSans', fontSize: 15),
              titleLarge: TextStyle(
                  fontFamily: 'AlbertSans', fontSize: 18, color: Colors.cyan),
              bodyLarge: TextStyle(fontFamily: 'AlbertSans', fontSize: 16),
              bodyMedium: TextStyle(fontFamily: 'AlbertSans', fontSize: 13),
              bodySmall: TextStyle(fontFamily: 'AlbertSans', fontSize: 11),
            ),
        appBarTheme: AppBarTheme(
          titleTextStyle: TextStyle(
            fontFamily: 'AlbertSans',
            fontSize: 19,
          ),
        ),
      ),
      home: const MyHomePage(title: 'App de Finazas Personal'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Transaction> _userTransactions = [
    Transaction(
      title: "Audifonos SkullCandy",
      amount: 99.6,
    ),
  ];

  List<Transaction> get _recentTransactions {
    return _userTransactions
        .where(
            (tx) => tx.date.isAfter(DateTime.now().subtract(Duration(days: 7))))
        .toList();
  }

  void _deleteTransaction(String id) {
    print("Deleting transaction $id");
    setState(() {
      _userTransactions.removeWhere((t) => t.id == id);
    });
  }

  void addTransaction(Transaction t) {
    print("Adding a new ${t.id} on ${t.date}");

    setState(() {
      _userTransactions.add(t);
    });
  }

  void startAddNewTransaction(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (bCtx) => GestureDetector(
              onTap: (() {}),
              behavior: HitTestBehavior.opaque,
              child: TransactionForm(
                addTrx: addTransaction,
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            onPressed: () => startAddNewTransaction(context),
            icon: Icon(Icons.add_box_outlined),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Chart(trxs: _recentTransactions),
          Expanded(
            child: ListViewTransactions(
              transactions: _userTransactions,
              deleteTrx: _deleteTransaction,
            ),
          ),
        ],
      ),
    );
  }
}
