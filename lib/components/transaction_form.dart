// ignore_for_file: prefer_const_constructors_in_immutables, avoid_print, prefer_const_constructors

import 'package:flutter/material.dart';

import '../classes/transaction.dart';

class TransactionForm extends StatefulWidget {
  final Function(Transaction) addTrx;

  TransactionForm({Key? key, required this.addTrx}) : super(key: key);

  @override
  State<TransactionForm> createState() => _TransactionFormState();
}

class _TransactionFormState extends State<TransactionForm> {
  final TextEditingController _titleC = TextEditingController();
  final TextEditingController _amountC = TextEditingController();
  DateTime? _selectedDate;

  void _submitTrx() {
    // volue is not required with the controllers
    final enteredTitle = _titleC.text;
    final enteredAmount = double.parse(_amountC.text);

    if (enteredTitle.isNotEmpty && enteredAmount >= 0) {
      print("Aceptado");
      widget.addTrx(Transaction(
          title: enteredTitle, amount: enteredAmount, date: _selectedDate));
    }
    Navigator.of(context).pop();
  }

  void _selectDate() async {
    var date = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021),
      lastDate: DateTime.now(),
    );

    print(date?.toUtc);

    setState(() {
      _selectedDate = date;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                "Ingresando una nueva compra",
                style: Theme.of(context).textTheme.titleLarge,
              )),
          TextField(
            decoration: const InputDecoration(labelText: "Titulo de la compra"),
            controller: _titleC,
          ),
          TextField(
            decoration: const InputDecoration(labelText: "Monto de la compra"),
            controller: _amountC,
            keyboardType: TextInputType.number,
          ),
          SizedBox(
            height: 45,
            child: Row(
              children: [
                Expanded(
                  child: Text(_selectedDate == null
                      ? "No se seleccion fecha"
                      : "${_selectedDate?.day}/${_selectedDate?.month}/${_selectedDate?.year}"),
                ),
                TextButton(
                  onPressed: _selectDate,
                  child: Text("Elegir Fecha"),
                ),
              ],
            ),
          ),
          ElevatedButton(onPressed: _submitTrx, child: Text("Guardar"))
        ],
      ),
    );
  }
}
