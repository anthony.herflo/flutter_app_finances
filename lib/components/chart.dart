// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables, avoid_print

import 'package:app_finanza_personal/classes/transaction.dart';
import 'package:flutter/material.dart';

class Chart extends StatelessWidget {
  final List<Transaction> trxs;

  Chart({Key? key, required this.trxs}) : super(key: key);

  static const List<String> daysInitials = ['L', 'M', 'X', 'J', 'V', 'S', 'D'];

  List<Map<String, Object>> get groupedTransactionValues {
    final amountsPerDay = List.generate(7, (dayIndex) {
      final weekDay = DateTime.now().subtract(Duration(days: dayIndex));
      double totalAmount = 0.0;

      for (var tx in trxs) {
        if (tx.date.day == weekDay.day &&
            tx.date.month == weekDay.month &&
            tx.date.year == weekDay.year) {
          totalAmount += tx.amount;
        }
      }

      return {
        "day": daysInitials[weekDay.weekday - 1],
        "amount": totalAmount.roundToDouble()
      };
    });

    return amountsPerDay.reversed.toList();
  }

  @override
  Widget build(BuildContext context) {
    double maxSpend = groupedTransactionValues.fold(
        0.0, (prev, e) => (e['amount'] as double) + prev);

    print(groupedTransactionValues);
    return Card(
      elevation: 6,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 6),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: groupedTransactionValues
              .map((e) => ChartBar(
                    maxSpend: maxSpend,
                    day: e['day'].toString(),
                    amount: e['amount'] as double,
                  ))
              .toList(),
        ),
      ),
    );
  }
}

class ChartBar extends StatelessWidget {
  final double maxSpend;
  final String day;
  final double amount;

  ChartBar({
    Key? key,
    required this.maxSpend,
    required this.day,
    required this.amount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FittedBox(fit: BoxFit.fitWidth, child: Text("${amount.round()} S/.")),
        Container(
          height: 110,
          width: 10,
          margin: EdgeInsets.symmetric(vertical: 6),
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                      color: Theme.of(context).primaryColor, width: 1.0),
                  color: Color.fromARGB(80, 100, 100, 100),
                  borderRadius: BorderRadius.all(Radius.circular(9)),
                ),
              ),
              FractionallySizedBox(
                heightFactor: amount / (maxSpend > 0 ? maxSpend : 1),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(9)),
                      color: Theme.of(context).primaryColor),
                ),
              )
            ],
          ),
        ),
        Text(day, style: Theme.of(context).textTheme.titleLarge),
      ],
    );
  }
}
