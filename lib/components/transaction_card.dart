import 'package:flutter/material.dart';

class TransactionCard extends StatelessWidget {
  final String id;
  final String title;
  final double amount;
  final DateTime date;
  final void Function(String) deleteTrx;

  const TransactionCard(
      {Key? key,
      required this.id,
      required this.title,
      required this.amount,
      required this.date,
      required this.deleteTrx})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        width: 90,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            width: 2,
            color: Theme.of(context).primaryColor,
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
        child: FittedBox(
          child: Text(
            "${amount.toStringAsFixed(2)} S/.",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor),
          ),
        ),
      ),
      title: Text(title),
      subtitle: Text("${date.day}/${date.month}/${date.year}"),
      trailing: IconButton(
          onPressed: (() {
            deleteTrx(id);
          }),
          icon: Icon(
            Icons.delete,
            color: Theme.of(context).errorColor,
          )),
    );
  }
}
