import 'package:app_finanza_personal/classes/transaction.dart';
import 'package:app_finanza_personal/components/transaction_card.dart';
import 'package:flutter/material.dart';

class ListViewTransactions extends StatefulWidget {
  final List<Transaction> transactions;
  final void Function(String) deleteTrx;
  const ListViewTransactions(
      {Key? key, required this.transactions, required this.deleteTrx})
      : super(key: key);

  @override
  State<ListViewTransactions> createState() => _ListViewTransactionsState();
}

class _ListViewTransactionsState extends State<ListViewTransactions> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 5),
      height: 300,
      child: ListView.builder(
        itemCount: widget.transactions.length,
        itemBuilder: (context, index) => TransactionCard(
            id: widget.transactions[index].id,
            title: widget.transactions[index].title,
            amount: widget.transactions[index].amount,
            date: widget.transactions[index].date,
            deleteTrx: widget.deleteTrx),
      ),
    );
  }
}
